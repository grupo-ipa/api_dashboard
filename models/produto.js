'use strict';
module.exports = (sequelize, DataTypes) => {
  const Produto = sequelize.define('Produto', {
    nome: DataTypes.STRING,
    dataProducao: DataTypes.STRING,
    previsaoEnvase: DataTypes.STRING,
    envaseReal: DataTypes.STRING
  }, {});
  Produto.associate = function(models) {
    // associations can be defined here
  };
  return Produto;
};
