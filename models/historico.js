'use strict';
module.exports = (sequelize, DataTypes) => {
  const Historico = sequelize.define('Historico', {
    nome: DataTypes.STRING,
    tanqueId:DataTypes.INTEGER,
    atributo: DataTypes.STRING,
    produtoId: DataTypes.STRING,
    valor: DataTypes.STRING
  }, {});
  Historico.associate = function(models) {
    // associations can be defined here
  };
  return Historico;
};
