'use strict';
module.exports = (sequelize, DataTypes) => {
  const Atividade = sequelize.define('Atividade', {
    produtoId:  DataTypes.INTEGER,
    descricao:  DataTypes.STRING,
    dataAtividade: DataTypes.STRING,
    dataLimite: DataTypes.STRING,
    status:  DataTypes.INTEGER
  }, {});
  Atividade.associate = function(models) {
    // associations can be defined here
  };
  return Atividade;
};
