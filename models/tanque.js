'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tanque = sequelize.define('Tanque', {
    temperatura: DataTypes.FLOAT,
    ph: DataTypes.FLOAT,
    status: DataTypes.STRING,
    tempReal: DataTypes.FLOAT,
    tempPrev: DataTypes.FLOAT,
    co2: DataTypes.FLOAT,
    peso: DataTypes.FLOAT,
    produtoId: DataTypes.INTEGER
  }, {});
  Tanque.associate = function(models) {
    // associations can be defined here
  };
  return Tanque;
};
