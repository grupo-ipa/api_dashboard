const produtoController = require('../controllers').produto;
const atividadeController = require('../controllers').atividade;
const tanqueController = require('../controllers').tanque;
const historicoController = require('../controllers').historico

module.exports = (app) => {
    app.get('/api/produto' , function (req,res) {
        produtoController.findAll(req, res)});

    app.delete('/api/produto/:produtoId' ,  (req,res) => {
        produtoController.destroy(req, res);
        tanqueController.cleanTanque(req,res);
        atividadeController.destroyByProduct(req,res)
        historicoController.destroyByProduct(req, res)
        });

    app.get('/api/produto/:produtoId' , function (req,res) {
        produtoController.findById(req, res)});
        
    app.put('/api/produto/:produtoId' , function (req,res) {
        produtoController.update(req, res)});

    app.put('/api/atividades/:produtoId/:atividadeId' , function (req,res) {
        atividadeController.update(req, res)});
    

    app.post('/api/produto', function (req, res) {
        produtoController.create(req, res) })

    app.get('/api/atividades/:produtoId' , function (req,res) {
        atividadeController.findAll(req, res)});

    app.delete('/api/atividades/:atividadeId' , function (req,res) {
        atividadeController.destroy(req, res)});

    app.get('/api/atividades/:produtoId/:atividadeId' , function (req,res) {
        atividadeController.findById(req, res)});

    app.post('/api/atividades/:produtoId', function (req, res) {
        atividadeController.create(req, res)})

    app.post('/api/tanque', function (req, res) {
        tanqueController.create(req, res)});

    app.get('/api/tanque' , function (req,res) {
        tanqueController.findAll(req, res)});

    app.delete('/api/tanque/:tanqueId' , function (req,res) {
        tanqueController.destroy(req, res)});

    app.get('/api/tanque/:tanqueId' , function (req,res) {
        tanqueController.findById(req, res)});
        
    app.put('/api/tanque/:tanqueId' , function (req,res) {
        tanqueController.update(req, res)});

    app.post('/api/historico', function (req, res) {
        historicoController.create(req, res);
        tanqueController.updateSingleAtribute(req,res)});

    app.get('/api/historico/:tanqueId' , function (req,res) {
        historicoController.findAll(req, res)});

    app.delete('/api/historico/:historicoId' , function (req,res) {
        historicoController.destroy(req, res)});


    app.get('/api/historico/:tanqueId' , function (req,res) {
        historicoController.findById(req, res)});
    
    app.get('/api/ultimasAtividades' , function (req,res) {
        historicoController.findLastTen(req, res)});
        
    app.put('/api/tanque/:historicoId' , function (req,res) {
        historicoController.update(req, res)});
    
    app.get('/api/proximasAtividades/:produtoId', function (req,res) {
        atividadeController.findNearToEnd(req, res)});
}
