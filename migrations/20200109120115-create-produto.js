'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Produto', {
      Produto_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nome: {
        type: Sequelize.STRING
      },
      temperatura: {
        type: Sequelize.FLOAT
      },
      ph: {
        type: Sequelize.FLOAT
      },
      prev_temp: {
        type: Sequelize.FLOAT
      },
      real_temp: {
        type: Sequelize.FLOAT
      },
      co2: {
        type: Sequelize.FLOAT
      },
      peso: {
        type: Sequelize.FLOAT
      },
      init_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      prev_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      real_envase_date: {
        allowNull: false,
        type: Sequelize.DATE
      },
      tanques_id :{
        allowNull: false,
        type: Sequelize.INT
      }

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Alunos');
  }
};
