const produto = require('./produto');
const atividade = require('./atividade');
const tanque = require('./tanque');
const historico = require('./historico');
module.exports = {
    produto,
    atividade,
    tanque,
    historico
};