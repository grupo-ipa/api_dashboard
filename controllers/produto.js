const Produto = require('../models').Produto;
const tanqueController = require('../controllers').tanque;

module.exports = {
    create(req, res) {
       console.log( Produto.findAll({}))
        return Produto
        .create({
            nome: req.body.nome,
            dataProducao: req.body.dataProducao,
            previsaoEnvase: req.body.previsaoEnvase,
            envaseReal: req.body.envaseReal
        })
        .then(produto => res.status(201).send(produto))
        .catch(error => res.status(401).send(error));
    },
    update(req, res) {
          return Produto
          .update(

          // Set Attribute values
                {
                    nome: req.body.nome,
                    dataProducao: req.body.dataProducao,
                    previsaoEnvase: req.body.previsaoEnvase,
                    envaseReal: req.body.envaseReal
                },

          // Where clause / criteria
                 { where : {id : req.params.produtoId} }

         )
         .then(produto => res.status(201).send(produto))
         .catch(error => res.status(401).send(error));

      },

    destroy(req, res) {
        // .destroy({
        //     where: {
        //     id: req.params.produtoId
        //     }
        // }))
        // .then(produto => res.status(201).send(produto))
        Produto.findAll({
            where: {
                id:req.params.produtoId
            }
         }).then((result) => {
             return Produto.destroy({where: {
                 id:req.params.produtoId
             }})
                       .then((u) => {
                           if(result == ""){
                               console.log(result)
                               res.status(404).send(result)
                                }
                            else {
                                res.status(201).send(result);
                        }
                            }
                               );
         });
    },

    findAll(req,res) {
        return Produto
        .findAll({}).then(produto => res.send(produto))
    },

    findById(req,res) {
        return Produto
        .findAll({
            where :{
                id:req.params.produtoId
            }
        }).then(produto => res.send(produto))
    }
};
