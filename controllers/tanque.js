const Tanque = require('../models').Tanque;
module.exports = {
    create(req, res) {
        return Tanque
        .create({
            id: req.body.id,
            temperatura: req.body.temperatura,
            ph: req.body.ph,
            status: req.body.status,
            tempPrev: req.body.tempPrev,
            tempReal: req.body.tempReal,
            co2: req.body.co2,
            peso: req.body.peso,
            produtoId: req.body.produtoId
        })
        .then(tanque => res.status(201).send(tanque))
        .catch(error => res.status(401).send(error));
    },
    update(req, res) {
          return Tanque
          .update(

        // Set Attribute values
            {
                id: req.body.id,
                temperatura: req.body.temperatura,
                ph: req.body.ph,
                status: req.body.status,
                tempPrev: req.body.tempPrev,
                tempReal: req.body.tempReal,
                co2: req.body.co2,
                peso: req.body.peso,
                produtoId: req.body.produtoId
            },

        // Where clause / criteria
                { where : {id : req.params.tanqueId} }

         )
         .then(tanque => res.status(201).send(tanque))
         .catch(error => res.status(401).send(error));

      },


    updateSingleAtribute (req,res) {
        let x = {};
        x[req.body.atributo] = req.body.valor;

        return Tanque
        .update(

      // Set Attribute values
          {
            temperatura: x.temperatura,
            ph: x.ph,
            status: x.status,
            tempPrev: x.tempPrev,
            tempReal: x.tempReal,
            co2: x.co2,
            peso: x.peso,
            produtoId: x.produtoId

          },

      // Where clause / criteria
              { where : {id : req.body.tanqueId} }

       )
    },
    destroy(req, res) {
        Tanque.findAll({
            where: {
                id:req.params.tanqueId
            }
         }).then((result) => {
             return Tanque.destroy({where: {
                 id:req.params.tanqueId
             }})
                       .then((u) => {
                           if(result == ""){
                               console.log(result)
                               res.status(404).send(result)
                                }
                            else {
                                res.status(201).send(result);
                        }
                            }
                               );
         });
    },

    cleanTanque(req,res){
        return Tanque
        .update(

            // Set Attribute values
                {
                    temperatura: null,
                    ph: null,
                    tempPrev: null,
                    tempReal: null,
                    status: null,
                    co2: null,
                    peso: null,
                    produtoId: null
                },{
            where: {
                produtoId: req.params.produtoId}
        })
    },

    findAll(req,res) {
        return Tanque
        .findAll({}).then(tanque => res.send(tanque))
    },

    findById(req,res) {
        return Tanque
        .findAll({
            where :{
                id:req.params.tanqueId
            }
        }).then(tanque => res.send(tanque))
    }
};
