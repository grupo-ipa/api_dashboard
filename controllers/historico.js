const Historico = require('../models').Historico;
module.exports = {
    create(req, res) {
        return Historico
        .create({
            nome: req.body.nome,
            tanqueId: req.body.tanqueId,
            produtoId: req.body.produtoId,
            atributo:req.body.atributo,
            valor:req.body.valor
        })
        .then(atividade => res.status(201).send(atividade))
        .catch(error => res.status(401).send(error));
    },
    update(req, res) {
          return Historico
          .update(

            // Set Attribute values
            {
                nome: req.body.nome,
                tanqueId: req.body.tanqueId,
                produtoId: req.body.produtoId,
                atributo:req.body.atributo,
                valor:req.body.valor
            },

          // Where clause / criteriaa
          {
            where :{
                id:req.params.historicoId
            }
        }).then(atividade => res.status(201).send(atividade))
         .catch(error => res.status(401).send(error));

      },

      destroy(req, res) {
            Historico.findAll({
                where: {
                    id:req.params.historicoId
                }
            }).then((result) => {
                return Historico.destroy({where: {
                    id:req.params.historicoId
                }})
                        .then((u) => {
                            if(result == ""){
                                console.log(result)
                                res.status(404).send(result)
                                    }
                                else {
                                    res.status(201).send(result);
                            }
                                }
                                );
            });
    },

    destroyByProduct(req, res) {
        return Historico
        .destroy({
            where: {
                produtoId: req.params.produtoId
            }
        })
    },

    findAll(req,res) {
        if(req.query.atributo){
            return Historico
            .findAll({
                where :[
                {
                    tanqueId: req.params.tanqueId
                },
                {
                    atributo: req.query.atributo
                }]}).then(produto => res.send(produto))
            }
        else {
            return Historico
            .findAll({
                where :
                {
                    tanqueId: req.params.tanqueId
                }}).then(produto => res.send(produto))
            }
        
    },

    findLastTen(req,res) {
        return Historico
        .findAll({}).then(produto => res.status(201).send((produto.reverse().slice(0,10))))
        
    },

    findById(req,res) {
        return Historico
        .findAll({
            where :{
                tanqueId:req.params.tanqueId
            }
        }).then(produto => res.send(produto))
    },



};
