const Atividade = require('../models').Atividade;
function predicateBy(prop){
    return function(a,b){
       if (a[prop] > b[prop]){
           return 1;
       } else if(a[prop] < b[prop]){
           return -1;
       }
       return 0;
    }
 }

module.exports = {
    create(req, res) {
        return Atividade
        .create({
            produtoId: req.params.produtoId,
            descricao: req.body.descricao,
            dataAtividade:req.body.dataAtividade,
            dataLimite:req.body.dataLimite,
            status: req.body.status
        })
        .then(atividade => res.status(201).send(atividade))
        .catch(error => res.status(401).send(error));
    },
    update(req, res) {
          return Atividade
          .update(

            // Set Attribute values
            {
                produtoId: req.body.produtoId,
                descricao: req.body.descricao,
                dataAtividade:req.body.dataAtividade,
                dataLimite:req.body.dataLimite,
                status: req.body.status
            },

          // Where clause / criteriaa
          {
            where :[{
                id:req.params.atividadeId
            },
            {
                produtoId: req.params.produtoId
            }]
        }).then(atividade => res.status(201).send(atividade))
         .catch(error => res.status(401).send(error));

      },

      destroy(req, res) {
        Atividade.findAll({
            where: {
                id:req.params.atividadeId
            }
         }).then((result) => {
             return Atividade.destroy({where: {
                 id:req.params.atividadeId
             }})
                       .then((u) => {
                           if(result == ""){
                               console.log(result)
                               res.status(404).send(result)
                                }
                            else {
                                res.status(201).send(result);
                        }
                            }
                               );
         });
    },
    destroyByProduct(req, res) {
        return Atividade
        .destroy({
            where: {
            produtoId: req.params.produtoId
            }
        })
    },

    findAll(req,res) {
        if(req.query.data){
            return Atividade
            .findAll({
                where :[
                {
                    produtoId: req.params.produtoId
                },
                {
                    dataAtividade: req.query.data
                }]}).then(produto => res.send(produto))
            }
        else {
            return Atividade
            .findAll({
                where :
                {
                    produtoId: req.params.produtoId
                }}).then(produto => {res.send(produto)})
            }
        
    },

    findById(req,res) {
        return Atividade
        .findAll({
            where :[{
                id:req.params.atividadeId
            },
            {
                produtoId: req.params.produtoId
            }]
        }).then(produto => res.send(produto))
    },

    findNearToEnd(req,res) {
        return Atividade
        .findAll({
            where :[
            {
                produtoId: req.params.produtoId
            }]
        }).then(produto => {
            console.log("AAAAÄAA")
            produto = produto.filter(x => x.status==0)
            for (let index = 0; index < produto.length; index++) {
                produto[index].dataLimite = new Date(produto[index].dataLimite)
            }
            produto = produto.sort(predicateBy("dataLimite"))
            
            res.send(produto);
        })
    }

};
